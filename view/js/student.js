window.onload = function(){
    fetch('/students')
        .then(response => response.text())
        .then(data => showStudents(data));
}

function newRow(student){
    var table = document.getElementById("myTable");
    var row = table.insertRow(table.length);
    var td = []

    for (i=0;i<table.rows[0].cells.length;i++){
        td[i] = row.insertCell(i);
    }

    td[0].innerHTML = student.stdid;
    td[1].innerHTML = student.fname;
    td[2].innerHTML = student.lname;
    td[3].innerHTML = student.email;
    td[4].innerHTML = `<input type="button" onclick="deleteStudent(this)" value="delete" id="button-1">`;
    td[5].innerHTML = '<input type="button" onclick="updateStudent(this)" value="edit" id="button-2">';
}

function addStudent(){
    var data = {
        stdid : parseInt(document.getElementById("sid").value),
        fname : document.getElementById("fname").value,
        lname : document.getElementById("lname").value,
        email : document.getElementById("email").value
    }
    
    var sid = data.stdid
    
    if(isNaN(sid)){
        alert("Enter valid student ID")
        return 
    }else if (data.email == ""){
        alert("Email cannotbe empty")
        return 
    }else if(data.fname == ""){
        alert("first name cannot be empty")
        return 
    }

    fetch('/student',{
        method:"POST",
        body: JSON.stringify(data),
        headers :{"content-type":"application/json;charset=UTF-8"}
    }).then((response1) => {
        if (response1.ok) {
            fetch('/student/'+sid)
            // Accessing the response body with the help of text() which further returns a promise.
            .then(Response2 => Response2.text())
            .then(data => showStudent(data))
        }else{
            throw new Error(response1.statusText)
        }
    }).catch(e => {alert(e)})

    resetform()
}

function showStudent(data) {
    const student = JSON.parse(data)
    newRow(student)
}

function resetform() {
    document.getElementById("sid").value = ""
    document.getElementById("fname").value = ""
    document.getElementById("lname").value = ""
    document.getElementById("email").value = ""
}

function showStudents(data) {
    const students = JSON.parse(data)
    
    students.forEach(stud => {
        newRow(stud)
    })
}

var selectedRow = null

function updateStudent(r){
    selectedRow = r.parentElement.parentElement

    document.getElementById("sid").value = selectedRow.cells[0].innerHTML;
    document.getElementById("fname").value = selectedRow.cells[1].innerHTML;
    document.getElementById("lname").value = selectedRow.cells[2].innerHTML;
    document.getElementById("email").value = selectedRow.cells[3].innerHTML;

    var btn = document.getElementById("button-add")
    sid = selectedRow.cells[0].innerHTML;

    if(btn){
        btn.innerHTML = "update";
        btn.setAttribute("onclick",`update(${sid})`)
    }
}

function getFormData(){
    var newData = {
        stdid : parseInt(document.getElementById("sid").value),
        fname : document.getElementById("fname").value,
        lname: document.getElementById("lname").value,
        email : document.getElementById("email").value
    }

    return newData
}

function update(sid) {
    var newData = getFormData()

    fetch("/student/"+sid,{
        method:"PUT",
        body:JSON.stringify(newData),
        header:{"content-type":"application/json; charset=UTF-8"}
    }).then (res => {
        if (res.ok) {
            selectedRow.cells[0].innerHTML = newData.stdid;
            selectedRow.cells[1].innerHTML = newData.fname;
            selectedRow.cells[2].innerHTML = newData.lname;
            selectedRow.cells[3].innerHTML = newData.email;

            var btn = document.getElementById("button-add");
            if(btn) {
                btn.innerHTML = "Add";
                btn.setAttribute("onclick","addStudent()");
                selectedRow = null;
                resetform();
            }else{
                alert("server:Update request error.")
            }
        }
    })
}

function deleteStudent(r){
    if (confirm("Are you sure you want to DELETE this?")){
        selectedRow = r.parentElement.parentElement;
        sid = selectedRow.cells[0].innerHTML;

        fetch('/student/'+sid,{
            method:"DELETE",
            headers:{"content-type":"application/json; charset=UTF-8"}
        });

        var rowIndex = selectedRow.rowIndex;
        if(rowIndex>0){
            document.getElementById("myTable").deleteRow(rowIndex);
            selectedRow = null
        }
    }
}
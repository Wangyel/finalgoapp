package controller

import (
	"database/sql"
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddCourse(w http.ResponseWriter, r *http.Request) {

	var course model.Course

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&course)

	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json data")
		return
	}

	saveErr := course.Create()

	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	} else {
		httpResp.RespondWithJSON(w, http.StatusCreated, map[string]string{"Message": "Course data add"})
		return
	}
}

func getCourseId(value string) (int64, error) {
	cid, err := strconv.ParseInt(value, 10, 64)
	return cid, err
}

func GetCourse(w http.ResponseWriter, r *http.Request) {

	cid := mux.Vars(r)["cid"]
	// cid, _ := getCourseId(cidStr)

	course := model.Course{Cid: cid}

	getErr := course.Read()

	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Course not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
	} else {
		httpResp.RespondWithJSON(w, http.StatusOK, course)
	}
}

func UpdateCourse(w http.ResponseWriter, r *http.Request) {
	cid := mux.Vars(r)["cid"]
	// cid, _ := getCourseId(cidStr)

	var course model.Course
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&course)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "Invalid json")
		return
	}

	UpdateErr := course.Update(cid)

	if UpdateErr != nil {
		switch UpdateErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Course not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, UpdateErr.Error())
		}
	} else {
		httpResp.RespondWithJSON(w, http.StatusOK, course)
	}
}

func DeleteCourse(w http.ResponseWriter, r *http.Request) {
	cid := mux.Vars(r)["cid"]
	// cid, idErr := getUserId(cidStr)

	// if idErr != nil {
	// 	httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
	// 	return
	// }

	course := model.Course{Cid: cid}
	err := course.Delete()

	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, map[string]string{"status": "deleted"})
}

func GetAllCourses(w http.ResponseWriter, r *http.Request) {
	courses, getErr := model.GetAllCourses()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJSON(w, http.StatusOK, courses)
}

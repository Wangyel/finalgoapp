package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAdmLogin(t *testing.T) {

	// REQUEST
	url := "http://localhost:8000/login"
	var jsonStr = []byte(`{"Email":"1222004.gcit@rub.edu.bt","Password":"T@10punakha"}`)

	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	// REQUESTER
	client := &http.Client{}

	// RESPON
	resp, err := client.Do(req)

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)

	assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	expResp := `{"error": "sql: no rows in result set"}`

	assert.JSONEq(t, expResp, string(body))
}

func TestAdmLogin2(t *testing.T) {

	// REQUEST
	url := "http://localhost:8000/login"
	var jsonStr = []byte(`{"Email":"12220043.gcit@rub.edu.bt","Password":"T@10punakha"}`)

	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	// REQUESTER
	client := &http.Client{}

	// RESPON
	resp, err := client.Do(req)

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)

	assert.Equal(t, http.StatusOK, resp.StatusCode)
	expResp := `{"Message": "success"}`

	assert.JSONEq(t, expResp, string(body))
}

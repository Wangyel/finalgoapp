package routes

import (
	"log"
	"myapp/controller"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func InitializeRoutes() {
	//Creating a new router
	router := mux.NewRouter()

	// register handler function with the mux router
	// The bellow two are api
	router.HandleFunc("/home", controller.HomeHandler)
	router.HandleFunc("/home/{name}", controller.ParameterHandler)
	router.HandleFunc("/student", controller.AddStudent).Methods("POST")
	router.HandleFunc("/student/{sid}", controller.GetStud).Methods("GET")
	router.HandleFunc("/student/{sid}", controller.UpdateStud).Methods("PUT")
	router.HandleFunc("/student/{sid}", controller.DeleteStud).Methods("DELETE")
	router.HandleFunc("/students", controller.GetAllStuds).Methods("GET")

	// COURSE ROUTE
	router.HandleFunc("/course", controller.AddCourse).Methods("POST")
	router.HandleFunc("/course/{cid}", controller.GetCourse).Methods("GET")
	router.HandleFunc("/course/{cid}", controller.UpdateCourse).Methods(("PUT"))
	router.HandleFunc("/course/{cid}", controller.DeleteCourse).Methods("DELETE")
	router.HandleFunc("/courses", controller.GetAllCourses)

	// ADMIN ROUTE
	router.HandleFunc("/signup", controller.Signup).Methods("POST")
	router.HandleFunc("/login", controller.Login).Methods("POST")
	router.HandleFunc("/logout", controller.Logout)

	// ENROLL ROUTE

	router.HandleFunc("/enroll", controller.Enroll).Methods("POST")
	router.HandleFunc("/enroll/{sid}/{cid}", controller.GetEnroll).Methods("GET")
	router.HandleFunc("/enroll/{sid}/{cid}", controller.DeleteEnroll).Methods("DELETE")
	router.HandleFunc("/enrolls", controller.GetEnrolls)

	// Setup for serving static file
	fileHandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fileHandler)

	// Start the http server
	err := http.ListenAndServe(":8000", router)
	if err != nil {
		os.Exit(1)
	}
	log.Println("Application running on port 8000")
	// Equivalent to line 12-15
	// log.Fatal(http.ListenAndServe(":8080",router))
}

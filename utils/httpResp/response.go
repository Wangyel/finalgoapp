package httpResp

import (
	"encoding/json"
	"net/http"
)

func RespondWithError(w http.ResponseWriter, code int, message string) {
	RespondWithJSON(w, code, map[string]string{"error": message})
}

func RespondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	// Go object to json fromat
	response, _ := json.Marshal(payload)
	// Response Header
	w.Header().Set("Content-Type", "application/json")
	// Header Status code
	w.WriteHeader(code)
	// Response Body
	w.Write(response)
}

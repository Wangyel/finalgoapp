package model

import "myapp/dataStore/postgres"

type Course struct {
	Cid        string `json:"cid"`
	CourseName string `json:"courseName"`
}

const (
	queryInsertCourse = "INSERT INTO course(cid,coursename) VALUES ($1,$2);"
	queryGetCourse    = "SELECT * FROM course WHERE cid=$1;"
	queryUpdateCourse = "UPDATE course SET cid=$1, coursename=$2 WHERE cid=$3 RETURNING cid;"
	queryDeleteCourse = "DELETE FROM course WHERE cid=$1 RETURNING cid;"
)

func (c *Course) Create() error {
	_, err := postgres.Db.Exec(queryInsertCourse, c.Cid, c.CourseName)
	return err
}

func (c *Course) Read() error {
	return postgres.Db.QueryRow(queryGetCourse, c.Cid).Scan(&c.Cid, &c.CourseName)
}

func (c *Course) Update(old_cid string) error {
	return postgres.Db.QueryRow(queryUpdateCourse, c.Cid, c.CourseName, old_cid).Scan(&c.Cid)
}

func (c *Course) Delete() error {
	err := postgres.Db.QueryRow(queryDeleteCourse, c.Cid).Scan(&c.Cid)
	return err
}

func GetAllCourses() ([]Course, error) {
	rows, getErr := postgres.Db.Query(("SELECT * FROM course;"))
	if getErr != nil {
		return nil, getErr
	}

	courses := []Course{}

	for rows.Next() {
		var c Course
		dbErr := rows.Scan(&c.Cid, &c.CourseName)
		if dbErr != nil {
			return nil, dbErr
		}

		courses = append(courses, c)
	}
	rows.Close()
	return courses, nil
}
